package lt.saltyjuice.dragas.xpathclaw;

import org.apache.camel.Exchange;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.lang.model.util.Elements;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class LinkGeneratingProcessor implements org.apache.camel.Processor {
    private final List<String> visited = Collections.synchronizedList(new ArrayList<>());
    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    public void process(Exchange exchange) throws Exception {
        System.err.println(String.format("Traversing: %s", exchange.getIn().getHeader(Exchange.HTTP_URI)));
        InputStream stream = exchange.getIn().getBody(InputStream.class);
        Document doc = Jsoup.parse(stream, "UTF-8", System.getProperty("xpathclaw.url"));
        List<String> e = doc
                .select("a")
                .stream()
                .map(it -> it.attr("href"))
                .filter((it) -> !visited.contains(it))
                .collect(Collectors.toList());
        visited.addAll(e);
        exchange.getOut().setBody(e);
    }
}
