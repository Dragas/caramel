package lt.saltyjuice.dragas.xpathclaw;

import org.apache.camel.Exchange;
import org.apache.camel.Expression;
import org.apache.camel.builder.RouteBuilder;

import java.util.function.Function;

//import static org.apache.camel.language.simple.types.BinaryOperatorType.ParameterType.Function;

public class HTTPRouteBuilder extends RouteBuilder {

    /**
     * <b>Called on initialization to build the routes using the fluent builder syntax.</b>
     * <p/>
     * This is a central method for RouteBuilder implementations to implement
     * the routes using the Java fluent builder syntax.
     *
     * @throws Exception can be thrown during configuration
     */
    public void configure() throws Exception {
        LinkGeneratingProcessor proc = new LinkGeneratingProcessor();
        from("direct:crawl")
                .to("http:memes")
                .process(proc)
                .split(body())
                .parallelProcessing()
                .setHeader(Exchange.HTTP_URI, body())
                .setBody(new Function<Exchange, Object>() {
                    /**
                     * Applies this function to the given argument.
                     *
                     * @param exchange the function argument
                     * @return the function result
                     */
                    @Override
                    public Object apply(Exchange exchange) {
                        return null;
                    }
                })
                .choice()
                    .when(header(Exchange.HTTP_URI).startsWith("http://file."))
                        .to("http:memes")
                        .process(new FilenameGeneratingProcessor())
                        .toD("file:./dir/${header.filename}")
                    .otherwise().to("direct:crawl");
    }
}
