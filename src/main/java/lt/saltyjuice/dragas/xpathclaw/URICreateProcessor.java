package lt.saltyjuice.dragas.xpathclaw;

import org.apache.camel.Exchange;

public class URICreateProcessor implements org.apache.camel.Processor {
    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String uri = exchange.getIn().getBody(String.class);
        exchange.getOut().setHeader(Exchange.HTTP_URI, uri);
    }
}
