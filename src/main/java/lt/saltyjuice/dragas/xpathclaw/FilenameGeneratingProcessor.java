package lt.saltyjuice.dragas.xpathclaw;

import org.apache.camel.Exchange;

public class FilenameGeneratingProcessor implements org.apache.camel.Processor {
    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String uri = exchange.getIn().getHeader(Exchange.HTTP_URI, String.class);
        String[] tree = uri.split("/");
        String filename = tree[tree.length - 1];
        exchange.getOut().setBody(exchange.getIn().getBody());
        exchange.getOut().setHeader("filename", filename);
    }
}
