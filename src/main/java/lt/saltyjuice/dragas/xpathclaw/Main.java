package lt.saltyjuice.dragas.xpathclaw;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws Exception {
        CamelContext ctx = new DefaultCamelContext();
        ctx.addRoutes(new HTTPRouteBuilder());
        ctx.start();
        ProducerTemplate t = ctx.createProducerTemplate();
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_URI, System.getProperty("xpathclaw.url"));
        t.sendBodyAndHeaders("direct:crawl", null, headers);
        Thread.yield();
    }
}
